<?php
    $page_title = "Press Articles";
    include ( "header.inc" );
    
class PressList {
    var $list;

    function addLink($date, $text, $url)
    {
        if ($url)
            $this->list[] = "<li>$date&nbsp;<a href=\"$url\">$text</a></li>";
        else
            $this->list[] = "<li>$date&nbsp;$text</li>";
    }

    function show()
    {
      echo "<ul>\n";
      foreach($this->list as $item)
	  echo $item."\n";
      echo "</ul>\n";
    }
};

$p = new PressList;

$p->addLink("2012/10/21"
	    , "How To Install Umbrello (UML Modeller) On Windows 7"
	    , "http://sidiq.mercubuana-yogya.ac.id/how-to-install-umbrello-uml-modeller-on-windows-7/"
	    );
$p->addLink("2012/04/05"
	    , "Basics of Umbrello UML Modeller"
	    , "http://www.zbeanztech.com/blog/basics-umbrello-uml-modeller"
	    );
$p->addLink("2009/09/17"
	    , "Native MacOS Umbrello in KDE 4"
	    , ""
	    );
$p->addLink("2008/29/01"
	    , "Penguin-powered UML modeling (The Register)"
	    , "http://www.theregister.co.uk/2008/01/29/linux_uml_modeling/"
	    );
$p->addLink("2003/06/01"
	    , "Software modelling in (German) linux magazine"
	    , "http://www.linux-magazin.de/Ausgaben/2003/06/Gut-beschirmt/%28language%29/ger-DE"
	    );
$p->addLink("2003/04/01"
	    , "Jonathan Riddell - BSc Honours Dissertation, Final Report"
	    , ""
	    );
$p->addLink(""
	    , "Wikipedia article"
	    , "https://de.wikipedia.org/wiki/Umbrello"
	    );
$p->show();

include ( "footer.inc" );
?>
