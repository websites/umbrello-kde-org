<?php

$verbose=0;
$product = "Umbrello";
$pathRoot="home:rhabacker:branches:windows:mingw";
$urlRoot="https://download.opensuse.org/repositories/";
$repoRoot="openSUSE_Leap_15.1";
$products = array(
    array(
        "Umbrello (stable)",
        ":snapshots",
        "umbrello",
        "https://invent.kde.org/sdk/umbrello/-/commits/release/20.04/"
    ),
);

$archs = array( "32", "64");


function getProjectPath($arch,$product)
{
    global $pathRoot;
    return "$pathRoot:win$arch".$product[1]."/mingw$arch-$product[2]";
}

function getProjectsList($prefix, $suffix)
{
    global $products,$archs;
    $s = "";
    foreach($products as $product) {
        foreach($archs as $arch) {
            $s .= $prefix.getProjectPath($arch,$product).$suffix;
        }
    }
    return $s;
}

function getUrl($arch,$subpath)
{
    global $pathRoot,$urlRoot,$repoRoot;
    return $urlRoot.str_replace(":",":/","$pathRoot:win$arch$subpath")."/$repoRoot/noarch/";
}

header("Content-Type: text/html; charset=utf-8");

echo "<!DOCTYPE html>"
    . "<html><body style=\"background-color:white;\">"
    . "<h1>Download cross compiled $product snapshots</h1>"
    ;

foreach($products as $product) {
    $productName = $product[0];
    $path = $product[1];
    $baseName = $product[2];
    $gitUrl = $product[3];
    echo "<h2>Product: $productName</h2>"
        ."<a href=\"".$gitUrl."\">Changelog</a><br/>"
        ."<br/>"
        ;

    foreach($archs as $arch) {
        $url = getUrl($arch, $path);
        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $page = curl_exec($ch);
            curl_close($ch);
            if ($verbose == 1)
                echo "<small>$url</small>";
            if ($verbose > 1)
                echo "<small>$page</small>";
            $lines = explode("\n", $page);
            $portableLink = "";
            $setupLink = "";
            $debugPackageLink = "";
            $portableText = "portable package";
            $setupText = "setup installer";
            $debugPackageText = "debug symbol package";

            foreach($lines as $line) {
                # extract link
                #  href="mingw32-umbrello-portable-4.8.7f3e468f9-75.3.noarch.rpm"><..."
                #  href="mingw64-umbrello-portable-4.8.3cf78f9c1-lp151.76.16.noarch.rpm"><..."
                preg_match("/<a href=\"(.*$baseName-.*\.rpm)\"></", $line, $matches);
                if (sizeof($matches) > 0) {
                    #echo "+++<pre>".$matches[1]."</pre>+++";
                    if (strstr($matches[1], "portable") !== FALSE)
                        $portableLink = $matches[1];
                    if (strstr($matches[1], "setup") !== FALSE)
                        $setupLink = $matches[1];
                    if (strstr($matches[1], "debugpackage") !== FALSE)
                        $debugPackageLink = $matches[1];
                }
            }
            echo "$arch bit<br/>";
            if ($portableLink != '') {
                echo "<ul>"
                    . "<li><a href=\"$url$portableLink\">$portableText</a>&nbsp;compressed archive with portable installation</li>"
                    . "<li><a href=\"$url$setupLink\">$setupText</a>&nbsp;Executable (requires Administrator access)</li>"
                    . "<li><a href=\"$url$debugPackageLink\">$debugPackageText</a>&nbsp;compressed archive [1]</li>"
                    . "</ul>"
                    ;
            } else {
                echo "<ul><li>No snapshots available yet.</li></ul>";
            }
        } else {
            echo "<ul>"
                . "<li>choose <b>mingw$arch-$baseName-portable-....rpm</b> to get a zip file with a portable installation of $productName</li>"
                . "<li>choose <b>mingw$arch-$baseName-setup-....rpm</b> to get a setup installer for $productName</li>"
                . "<li>Debug symbols can be found in the file <b>mingw$arch-$baseName-debugpackage-....rpm</b>. They need to be unpacked in the directory above the <b>bin</b> folder
                so that gdb can find them.</li>"
                . "</ul>"
                . "<p>After clicking on the button below you will be redirected to the related download page to get a $productName snapshot</p>"
                . "<input type=\"button\" name=\"xxx\" value=\"$arch-bit snapshot\" onclick=\"javascript:window.location='$url';\">"
                ;
        }
    }
}
echo "<p>The rpm container can be unpacked with <a href=\"https://www.7-zip.de/\">7-zip</a>.</p>"
     ."<p>[1]&nbsp;Debug symbols need to be unpacked in the directory above the <b>bin</b> folder so that gdb can find them.</p>"
     ."</body></html>"
    ;
echo "
<h1>Local build instructions</h1>
To build the above mentioned packages on a local openSUSE Linux distribution you can follow this recipe:
<ul>
<li>create <a href=\"https://idp-portal.suse.com/univention/self-service/#page=createaccount\">obs account</a>, if not present</li>
<li>install osc:<pre>sudo zypper install osc</pre></li>
<li>checkout package:<pre>".getProjectsList("osc co ","\n\n")."</pre></li>
<li>build:<pre>".getProjectsList("cd ","\nosc build $repoRoot\n\n")."# Append -jx to build with x parallel threads</pre></li>
<li>enter build dir:<pre>osc chroot $repoRoot\ncd /home/abuild/rpmbuild/BUILD/</pre></li>
";
