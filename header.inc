<?php
    // add path to installed capacity framework when hosted on sourceforge
    if (isset($_SERVER['SF_PATH'])) {
        $path = $_SERVER['SF_PATH'].'/htdocs/capacity';
        if (is_dir($path)) {
            set_include_path(get_include_path() .':'. $path);
        }
    }

    if (file_exists("functions.inc")) {
        include_once ("functions.inc");
    }
    $rss_feed_link = "https://".$_SERVER['SERVER_NAME']."/rss.php";
    $rss_feed_title = "Latest News";
    include('includes/header.inc');
    if ($templatepath != "aether/") {
        // use local copies of template files
        unset($templatepath);
    }
    // set additional trademark which is displayed in the local template-bottom2.inc
    $trademarks = "Windows<sup>&#174;</sup> is a registered trademark of Microsoft Corporation in the United States and other countries. ";
?>
