<?php
  $page_title = "Umbrello Screenshots";
  include("header.inc");
  ?>

<?php
	echo "<p>Please send contributions of screenshots to jr @ jriddell.org.</p>";
	echo "<table>";
	echo "<tr><td>";
	screenshot("umbrello-2013-wee.png","umbrello-2013.png", true, "Umbrello from KDE SC 4.11");
	echo "</td><td>";
	echo "&nbsp;";
	echo "</td></tr>";
	echo "<tr><td>";
	screenshot("umbrello2.3.4_screenshot-wee.jpg","umbrello2.3.4_screenshot.jpg", true, "Umbrello from 2013");
	echo "</td><td>";
	screenshot("umbrello2-windows-wee.jpg","umbrello2-windows.jpg", true, "Umbrello 2 on Windows");
	echo "</td></tr>";
	echo "<tr><td>";
	screenshot("umbrello-2.0-wee.png","umbrello-2.0.png", true, "Umbrello 2.0!");
	echo "</td><td>";
	screenshot("umbrello2-mac-wee.png","umbrello2-mac.png", true, "Umbrello 2 on Mac OS X");
	echo "</td></tr>";
	echo "<tr><td>";
	screenshot("sak-wee.png","sak.png", true, "Screenshot from Marcelo Guillermo Diaz");	
	echo "</td><td>";
	screenshot("mvcbasic-sequence-wee.jpg","mvcbasic-sequence.jpg", true, "Screenshot from burnin");
	echo "</td></tr>";
	echo "<tr><td>";
	screenshot("umbrello-elcuco1-wee.png","umbrello-elcuco1.png", true, "Modelling a system for managing a cellular company in Hebrew");
	echo "</td><td>";
	screenshot("umbrello-ubuntu-wee.png","umbrello-ubuntu.png", true, "A happy Ubuntu Umbrello user.");
	echo "</td></tr>";
	echo "<tr><td>";
	screenshot("umbrello-entiry-relationship-wee.png","umbrello-entiry-relationship.png", true, "Umbrello 1.4 (KDE 3.4) features entity-relationship diagrams and tabbed diagram layout");
	echo "</td><td>";
	screenshot("ss_umbrello1-wee.png","ss_umbrello1.png", true, "Umbrello 1.4 used by the Mars game http://mars.sf.net");
	echo "</td></tr>";
	echo "<tr><td>";
	screenshot("screenshot-santiago-wee.png","screenshot-santiago.png", true, "A use case class diagram in Umbrello 1.3.");
	echo "</td><td>";
	screenshot("screenshot-santiago-2-wee.png","screenshot-santiago-2.png", true, "A class diagram in Umbrello 1.3.");
	echo "</td></tr>";
	echo "<tr><td>";
	screenshot("jose-wee.png","jose.png", true, "A class diagram in Umbrello 1.3.");
	echo "</td><td>";
	screenshot("screenshot-santiago-1-wee.png","screenshot-santiago-1.png", true, "A use case diagram in Umbrello 1.3 from Santiago Exequiel Ibarra.");
	echo "</td></tr>";
	echo "<tr><td>";
	screenshot("activity-diagram-1-wee.png","activity-diagram-1.png", true, "An activity diagram");
	echo "</td><td>";
	screenshot("activity-diagram-2-wee.png","activity-diagram-2.png", true, "Another activity diagram");
	echo "</td></tr>";
	echo "<tr><td>";
	screenshot("umbrello-gnome-wee.png","umbrello-gnome.png", true, "Umbrello 1.2-beta happily running under GNOME.");
	echo "</td><td>";
	screenshot("umbrello-import-wee.jpg","umbrello-import.jpg", true, "Umbrello having imported some C++ classes.");
	echo "</td></tr>";
	echo "<tr><td>";
	screenshot("steamedpenguin-wee.png","steamedpenguin.png", true, "#kde-devel regular steampenguin was playing with Umbrello");
	echo "</td><td>";
	screenshot("ars-technica-wee.png","ars-technica.png", true, "ars technica used this in their KDE 3.2 review </br>http://www.arstechnica.com/reviews/004/software/kde-3.2/kde-3.2-08.html");
	echo "</td></tr>";
	echo "</table>";
?>

<?php
  include("footer.inc");
?>
