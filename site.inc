<?php

// promote title to use
$site_title = "Umbrello Project";
$site_logo = "media/images/kde.png";
$site_search = false;
$site_menus = 1;
$templatepath = "aether/";

$site_external = true;
$name = $_SERVER['SERVER_NAME']." Webmaster";
$mail = "jr@jriddell.org";
$showedit = false;

$rss_feed_link = "/rss.php";
$rss_feed_title = "Latest Umbrello News";

// some global variables
$kde_current_version = "4.11";
$legalLink = "http://www.kde.org/community/whatiskde/impressum.php";

# see https://sysadmin.kde.org/tickets/index.php?page=tickets&act=view&id=TAW-0114
$piwikSiteID = 11;
$piwikEnabled = true;

$menuright = array(
    "news.php" => "News",
    "features.php" => "Features",
    "installation.php" => "Download",
    "support.php" => "Support",
    "screenshots.php" => "Screenshots",
    "documentation.php" => "Documentation",
    "press.php" => "Press&nbsp;Article",
    "reports.php" => "User&nbsp;Reports",
    "developers.php" => "Developer&nbsp;Resources",
);

?>

