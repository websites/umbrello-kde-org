<?php
    $page_title = "Umbrello Features";
    include ( "header.inc" );
?>

<h2>Notes on importing Rose files</h2>
<br/>
<p>A Rose model may consist of just a single file with ending <b>.mdl</b>.
Import of such a file is straight forward, Umbrello will detect the file type by the extension <b>.mdl</b> and automatically switch to Rose import mode.</p>
<p>A Rose model may also consist of the main model (<tt>.mdl</tt>) file plus submodel files with ending <b>.cat</b> or <b>.sub</b>.<br/>
It is not possible to import a single <b>.cat</b> or <b>.sub</b> file; on importing, always specify the main model file (<b>.mdl</b>).<br/>
References to submodels are resolved by the importer as follows.<br/>
Rose uses <i>path variables</i> for referencing submodels.<br/>
Here is an example of a reference to a submodel:<br/>
<pre>
            (object Class_Category "org"
                is_unit         TRUE
                is_loaded       FALSE
                file_name       "$FRAMEWORK_PATH\\Shared Components\\org12.cat"
                quid            "3794F4C802EE")
</pre>
Umbrello maps the Rose path variables to environment variables.<br/>
In the example, Umbrello requires that an environment variable <tt>FRAMEWORK_PATH</tt> is defined and it points to the directory in which the subdirectory "Shared Components" is located.<br/>
The path shall be specified as an absolute path.<br/>
For example, if your main model is in the directory <tt>/home/user/rose</tt> then the directory<br/>
<tt>Shared Components</tt> could be at the absolute path<br/>
<tt>/home/user/rose/Shared Components</tt>.<br/>
The environment variable shall then be defined as follows:<br/>
<tt>export FRAMEWORK_PATH=/home/user/rose</tt>
</p>
<br/>
<h3>Related feature request<h3/>
<a href="https://bugs.kde.org/show_bug.cgi?id=81364">https://bugs.kde.org/show_bug.cgi?id=81364</a>
<p/>

<?php
    include ( "footer.inc" );
?>
