<?php 
$versions = array(
	"23.04" => array("search" => "&o2=regexp&v2=23.04|23.04.[0-7]|23.03.[89][0-9]",                 "mapped" => "2.38"),
	"22.12" => array("search" => "&o2=regexp&v2=22.12|22.12.[0-7]|22.11.[89][0-9]",                 "mapped" => "2.37"),
	"22.08" => array("search" => "&o2=regexp&v2=22.08|22.08.[0-7]|22.07.[89][0-9]",                 "mapped" => "2.35"),
	"22.04" => array("search" => "&o2=regexp&v2=22.04|22.04.[0-7]|22.03.[89][0-9]",                 "mapped" => "2.34"),
	"21.04" => array("search" => "&o2=regexp&v2=21.04|21.04.[0-7]|21.03.[89][0-9]",                 "mapped" => "2.33"),
	"20.08" => array("search" => "&o2=regexp&v2=20.08|20.08.[0-7]|20.07.[89][0-9]",                 "mapped" => "2.32"),
	"20.04" => array("search" => "&o2=regexp&v2=20.04|20.04.[0-7]|20.03.[89][0-9]",                 "mapped" => "2.31"),
	"19.12" => array("search" => "&o2=regexp&v2=19.12|19.12.[0-7]|19.11.[89][0-9]",                 "mapped" => "2.30"),
	"19.08" => array("search" => "&o2=regexp&v2=19.08|19.08.[0-7]|19.07.[89][0-9]",                 "mapped" => "2.29"),
	"19.04" => array("search" => "&o2=regexp&v2=19.04|19.04.[0-7]|19.03.[89][0-9]",                 "mapped" => "2.28"),
	"18.12" => array("search" => "&o2=regexp&v2=18.12|18.12.[0-7]|18.11.[89][0-9]",                 "mapped" => "2.27"),
	"18.08" => array("search" => "&o2=regexp&v2=18.08|18.08.[0-7]|18.07.[89][0-9]",                 "mapped" => "2.26"),
	"18.04" => array("search" => "&o2=regexp&v2=18.04|18.04.[0-7]|18.03.[89][0-9]",                 "mapped" => "2.25"),
	"17.12" => array("search" => "&o2=regexp&v2=17.12|17.12.[0-7]|17.11.[89][0-9]",                 "mapped" => "2.24"),
	"17.08" => array("search" => "&o2=regexp&v2=17.08|17.08.[0-7]|17.07.[89][0-9]",                 "mapped" => "2.23"),
	"17.04" => array("search" => "&o2=regexp&v2=17.04|17.04.[0-7]|17.03.[89][0-9]",                 "mapped" => "2.22"),
	"16.12" => array("search" => "&o2=regexp&v2=16.12|16.12.[0-7]|16.11.[89][0-9]",                 "mapped" => "2.21"),
	"16.08" => array("search" => "&o2=regexp&v2=16.08|16.08.[0-7]|16.07.[89][0-9]",                 "mapped" => "2.20"),
	"16.04" => array("search" => "&o2=regexp&v2=16.04|16.04.[0-7]|16.03.[89][0-9]",                 "mapped" => "2.19"),
	"15.12" => array("search" => "&o2=regexp&v2=15.12|15.12.[0-7]|15.11.[89][0-9]",                 "mapped" => "2.18"),
	"15.08" => array("search" => "&o2=regexp&v2=15.08|15.08.[0-7]|15.07.[89][0-9]",                 "mapped" => "2.17"),
	"15.04" => array("search" => "&o2=regexp&v2=15.04|15.04.[0-7]|15.03.[89][0-9]|14.12.[89][0-9]", "mapped" => "2.16"),
	"14.12" => array("search" => "&o2=regexp&v2=14.12|14.12.[0-7]|4.14.[89][0-9]|14.11.[89]]0-9]",  "mapped" => "2.15"),
	"4.14"  => array("search" => "&o2=regexp&v2=4.14|4.14.[0-7]|4.13.[89][0-9]",                    "mapped" => "2.14"),
	"4.13"  => array("search" => "&o2=regexp&v2=4.13|4.13.[0-7]|4.12.[89][0-9]",                    "mapped" => "2.13"),
	"4.12"  => array("search" => "&o2=regexp&v2=4.12|4.12.[0-7]|4.11.[89][0-9]",                    "mapped" => "2.12"),
	"4.10"  => array("search" => "&o2=regexp&v2=4.11|4.11.[0-7]|4.10.[89][0-9]",                    "mapped" => "2.11"),
	"" => array("search" => "", "mapped" => "")
);

function bugList($type,$version="",$options="")
{
	global $versions;
	$baseColumns = "&columnlist=component%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Creporter_realname%2Cassigned_to_realname%2Cbug_severity%2Cversion";
	$columns = array(
		"open" => $baseColumns,
		"resolved" => "&columnlist=component%2Cbug_severity%2Cbug_status%2Cresolution%2Cshort_desc%2Ccf_versionfixedin",
		"assigned" => $baseColumns."%2Ctarget_milestone",
		"todo" => $baseColumns."%2Ctarget_milestone",
	);
	
	$status = array(
		"resolved" => "&bug_status=RESOLVED&resolution=FIXED&resolution=DUPLICATE&resolution=WORKSFORME&resolution=MOVED&resolution=UPSTREAM&resolution=DOWNSTREAM",
		"open" => "&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO",
		"assigned" => "&bug_status=ASSIGNED",
		"todo" => "&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=REOPENED",
	);

	$url = "https://bugs.kde.org/buglist.cgi?product=umbrello"
			."&query_format=advanced"
			;

	$types = array(
		"allbugs" => "",
		"changelog" => "",
		"wishlist" => "&bug_severity=wishlist",
		"crash" => "&bug_severity=crash",
		"critical" => "&bug_severity=critical",
		"grave" => "&bug_severity=grave",
		"major" => "&bug_severity=major",
		"normal" => "&bug_severity=normal",
		"minor" => "&bug_severity=minor",
		"juniorjobs" => "&keywords=junior-jobs&keywords_type=allwords",
		"resolved" => "&bug_severity=crash&bug_severity=major&bug_severity=normal&bug_severity=minor&bug_severity=grave&bug_severity=critical",
	);

	$search = array(
		"fixed" => "&f2=cf_versionfixedin".$versions[$version]["search"],
		"assigned" => "&f2=target_milestone".$versions[$version]["search"],
		"todo" => "&f2=target_milestone".$versions[$version]["search"],
		"open" => $versions[$version]["search"],
	);

	$orders = array(
		"status" => "&order=bug_status%2Cpriority%2Cassigned_to%2Cbug_id",
		"severity" => "&order=bug_severity%2Cbug_status%2Cpriority%2Cassigned_to%2Cbug_id",
		"versionfixed" => "&order=cf_versionfixedin",
	);
	$urls = array(
		"wishlist" => $url.$types[$type].$status['open'].$columns['open'].$orders['status'],
		"crash" => $url.$types[$type].$status['open'].$columns['open'].$orders['status'],
		"major" => $url.$types[$type].$status['open'].$columns['open'].$orders['status'],
		"normal" => $url.$types[$type].$status['open'].$columns['open'].$orders['status'],
		"minor" => $url.$types[$type].$status['open'].$columns['open'].$orders['status'],
		"juniorjobs" => $url.$types[$type].$status['open'].$columns['open'].$orders['status'],
		"changelog" => $url.$types['changelog'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['versionfixed'],
		"openbugs" => $url.$types['allbugs'].$status['open'].$columns['open'].$search['open'].$orders['status'],
		"resolvedbugs" => $url.$types['resolved'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"resolvedcrashbugs" => $url.$types['crash'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"resolvedmajorbugs" => $url.$types['major'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"resolvednormalbugs" => $url.$types['normal'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"resolvedminorbugs" => $url.$types['minor'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"resolvedfeatures" => $url.$types['wishlist'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"assignedfeatures" => $url.$types['wishlist'].$status['assigned'].$columns['assigned'].$search['assigned'].$orders['status'],
		"todofeatures" => $url.$types['wishlist'].$status['todo'].$columns['todo'].$search['todo'].$orders['status'],
	);
	
	$url = "";
	if (isset($urls[$type])) {
		$url = $urls[$type];
		if ($options)
			$url .= "&". $options;
	}
	return $url;
}

function getContentList($filename)
{
    $file = file_get_contents($filename);
    $lines = explode("\n", $file);
    $content = "";
    foreach ($lines as $line) {
    $pos = strpos($line, "<h2");
    if  ($pos !== FALSE && $pos == 0) {
        $a = str_replace('<h2 id="', '<li><a href="#', $line);
        $a = str_replace('</h2>', '</a></li>', $a);
        $content .= $a."\n";
      }
    }
    if ($content != "")
        return "<h2>Table of content</h2>"."<ol>".$content."</ol>";
    return $content;
}
