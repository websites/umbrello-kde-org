<?php
    $page_title = "Umbrello Documentation";
    include ( "header.inc" );
    $doc_url="https://docs.kde.org/trunk5/";
?>
<h2>Umbrello UML Modeller Handbook</h2>

<p>The handbook contains information on Umbrello and the Unified Modelling Language (UML).</p>

<ul>
<li><strong><a
href="<?php echo $doc_url ?>en/umbrello/umbrello/index.html">en - Umbrello UML Modeller Handbook</a></strong></li>
</ul>
<ul>
<li><strong><a href="<?php echo $doc_url ?>da/umbrello/umbrello/index.html">da - Umbrello UML Modeller-håndbogen</a></strong></li>
<li><strong><a href="<?php echo $doc_url ?>de/umbrello/umbrello/">de - Umbrello UML Modeller Handbuch</a></strong></li>
<!-- <li><strong><a href="https://docs.kde.org/development/eo/kdesdk/umbrello/">eo - Manlibro de Umbrello UML Modeller</a></strong></li> -->
<li><strong><a href="<?php echo $doc_url ?>es/umbrello/umbrello/">es - Manual de Umbrello UML Modeller</a></strong></li>
<!-- <li><strong><a href="<?php echo $doc_url ?>et/umbrello/umbrello/">et - Umbrello käsiraamat</a></strong></li> -->
<li><strong><a href="<?php echo $doc_url ?>it/umbrello/umbrello/">it - Manuale di Umbrello UML Modeller</a></strong></li>
<li><strong><a href="<?php echo $doc_url ?>nl/umbrello/umbrello/">nl - Het handboek van Umbrello UML Modeller</a></strong></li>
<li><strong><a href="<?php echo $doc_url ?>pt/umbrello/umbrello/">pt - Manual do Umbrello UML Modeller</a></strong></li>
<li><strong><a href="<?php echo $doc_url ?>pt_BR/umbrello/umbrello/">pt_BR - Manual do Umbrello UML Modeller</a></strong></li>
<!-- <li><strong><a href="<?php echo $doc_url ?>ru/umbrello/umbrello/">ru - Руководство Umbrello UML Modeller</a></strong></li> -->
<li><strong><a href="<?php echo $doc_url ?>sv/umbrello/umbrello/">sv - Umbrello UML Modeller-utvecklarna</a></strong></li>
<li><strong><a href="<?php echo $doc_url ?>uk/umbrello/umbrello/">uk - Підручник з Umbrello UML Modeller</a></strong></li>
</ul>

<p>Please help translate Umbrello into your langauge by joining your <a href="http://i18n.kde.org">KDE internationalisation team.</a></p>

<?php
    include ( "footer.inc" );
?>
