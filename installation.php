<?php
  $page_title = "Installation";
  include_once("lib.inc");
  include_once("header.inc");
  echo getContentList(__FILE__);
?>

<h2 id="install-as-part-of_kde-software-compilation">Install as Part of KDE Software Compilation</h2>
<p>Umbrello is part of KDE Software Compilation which means it comes with all GNU/Linux distributions.
 You will be able to install it using your normal software installation app (Muon, Software Centre, Yast etc)
 or command line package manager (yum, up2date, yast, zypper, apt-get, emerge etc).</p>
<p>The package will be called <strong>umbrello</strong>, some older distros will package it as <strong>kdesdk</strong>.</p>

<h2 id="install-fedora">Fedora</h2>
<p>At least on fedora 20 it is also required to install the package <b>oxygen-icon-theme</b>, otherwise umbrello will not show any icons.</p>

<h2 id="install-freebsd">FreeBSD</h2>
<p>Umbrello is available from <a href="http://www.freebsd.org/ports/devel.html">FreeBSD ports</a>.</p>

<h2 id="install-linux-snap-package">Linux as snap package</h2>
<p>Umbrello is available on Linux as <a href="https://snapcraft.io/umbrello">snap package</a>.</p>

<h2 id="install-windows">Windows</h2>
<p>Umbrello installer and portable packages for 32bit and 64 bit Windows are available at the <a href="http://download.kde.org/stable/umbrello/latest/">KDE download mirror network</a>.</p>
<!--
<p>Umbrello on Windows is also part of the <a
href="http://windows.kde.org">KDE on Windows</a> distribution installed with the <a
href="http://download.kde.org/stable/kdewin/installer/kdewin-installer-gui-latest.exe.mirrorlist">KDE-Installer</a>.
-->
Please note that the KDE on Windows distribution does not contain the latest umbrello releases.</p>

<h2 id="install-mac">Mac OS X</h2>
<p>Umbrello is available for Mac OS X on <a href="https://www.macports.org/ports.php?by=name&substr=umbrello">MacPorts</a>.

<h2 id="install-source-code-linux">Source Code on Linux like operating systems</h2>
<p>The program has no platform specific code, so should compile on any system with KDE.</p>
<p>Here are the packages required for building Umbrello: </p>
<ul>
 <li><a href="http://www.cmake.org">CMake</a></li>
 <li>GNU make</li>
 <li>GNU C++ compiler (g++)</li>
 <li>libxslt, libxml2</li>
 <li>Qt5/KF5
   <ul>
     <li>Qt &gt;= 5.4, including development package</li>
     <li>KDE frameworks development package 5.x</li>
   </ul>
   On openSUSE Leap 15.1 use the following command to install required packages:
   <pre>sudo zypper in kio-devel libQt5PrintSupport-devel libqt5-qttools-devel \
   libqt5-qtsvg-devel libQt5WebKitWidgets-devel kdoctools-devel \
   kiconthemes-devel kdelibs4support-devel ktexteditor-devel</pre>
 </li>
 <li>KDE4
   <ul>
     <li>Qt &gt;= 4.7, including development package</li>
     <li>kdelibs development package 4.8 or newer</li>
   </ul>
 </li>
</ul>

<p>To install from source out of <a href="http://git-scm.com/">git</a>, enter the following commands in a shell:</p>
<pre>
mkdir -p $HOME/src
cd $HOME/src
git clone git://anongit.kde.org/umbrello  
cd umbrello
mkdir build
cd build
</pre>
<p>for building with Qt5/KF5 run:</p>
<pre>
cmake -DCMAKE_INSTALL_PREFIX=$HOME/umbrello -DBUILD_KF5=1 -DCMAKE_BUILD_TYPE=Debug ../
</pre>
<p>and on building for KDE4</p>
<pre>
cmake -DCMAKE_INSTALL_PREFIX=$HOME/umbrello -DBUILD_KF5=0 -DCMAKE_BUILD_TYPE=Debug ../
</pre>
<p><i>Check the cmake output for missing build requirements and install them</i></p>
<pre>
make
make install
</pre>
<p>If you are building on the basis of Qt4/KDE4 and get an error from <i>cmake</i>,
<pre>
    "RELEASE_SERVICE_VERSION_MICRO" "GREATER_EQUAL" "70"
      Unknown arguments specified
</pre>
then try commenting out the following lines in the main <a href="https://invent.kde.org/sdk/umbrello/-/blob/master/CMakeLists.txt">CMakeLists.txt</a> file:</p>
<pre>
if (RELEASE_SERVICE_VERSION_MICRO GREATER_EQUAL 70)
    math(EXPR UMBRELLO_VERSION_MINOR "${UMBRELLO_VERSION_MINOR}-1")
endif()
</pre>

<h3>Running the program from local installation<h3>
<h4>Qt5/KF5 builds in a KF5 environment</h4>

<p>running umbrello KF5 build in a KF5 environment requires to use the following commands.</p>

<pre>$HOME/umbrello/bin/umbrello5 &</pre>
<p>
If someone wants to add a desktop entry:
<ol>
<li>Open a terminal on your desktop</li>
<li>Create a file named umbrello.desktop</li>
<li>Edit, paste and save</li>
</ol>
</p>
<pre>
[Desktop Entry]
Categories=Application;Development;
Comment[es_ES]=Umbrello - UML Modeller
Comment=Umbrello - UML Modeller
Exec=$HOME/umbrello/bin/umbrello5
GenericName[es_ES]=Umbrello
GenericName=Umbrello
Icon=/home/&lt;USER HOME&gt;/umbrello/share/icons/hicolor/scalable/apps/umbrello.svgz
Name=Umbrello
StartupNotify=true
Terminal=false
X-KDE-SubstituteUID=false
</pre>

<h4>Qt5/KF5 builds in a KDE4 environment</h4>
<p>running umbrello KF5 build in a KDE4 environment requires to use the following commands.</p>
<pre>
eval `dbus-launch`
kdeinit5
$HOME/umbrello/bin/umbrello &
</pre>

<h4>under a KDE4 environment</h4>
<p>Before running umbrello, you need set the KDEDIRS environment variable with:</p>
<pre>
export KDEDIRS=$HOME/umbrello:$KDEDIRS
kbuildsycoca4
</pre>
<p>Run the following command from a shell:</p>
<pre>
$HOME/umbrello/bin/umbrello &
</pre>
<p>You may add the following commands to your .bashrc or similar login shell config file
 to have umbrello in the users system path:</p>
<pre>
export KDEDIRS=$HOME/umbrello:$KDEDIRS
export PATH=$HOME/umbrello/bin:$PATH
</pre>

<p>With that you can run umbrello simply by typing:</p>
<pre>
umbrello
</pre>

<p>Note: There are reports that there may be no tool bar icons after installing umbrello/KDE4 in a non default location.
If this happens to you, please install an official umbrello/KDE4 version with your package manager;
then start you just created umbrello.</p>

<h2 id="install-source-code-windows">Source Code on Windows</h2>
<p>Umbrello could be build on windows from source with the
 <a href="http://techbase.kde.org/Getting_Started/Build/Windows/emerge">emerge build system</a>.</p>

<h2 id="cross-compile-for-windows">Cross compile Umbrello for Windows</h2>
<p>Umbrello could be cross compiled for Windows from source with the help of the <a href="https://build.opensuse.org/">openSUSE Build Service (OBS)</a>
    provided by the package <a href="https://build.opensuse.org/package/show/windows%3Amingw%3Awin32/mingw32-umbrello">mingw32-umbrello</a> for 32bit builds and
    <a href="https://build.opensuse.org/package/show/windows%3Amingw%3Awin64/mingw64-umbrello">mingw64-umbrello</a> for 64bit builds.</p>
<p>Windows installer and portable packages are provided with <a href="https://build.opensuse.org/package/show/windows:mingw:win32/mingw32-umbrello:mingw32-umbrello-installer">mingw32-umbrello-installer</a>
and <a href="https://build.opensuse.org/package/show/windows:mingw:win32/mingw32-umbrello:mingw32-umbrello-installer">mingw64-umbrello-installer</a>.</p>
<p>The mentioned obs packages provide remote builds by default. Local builds are possible on
    any Linux system with the help of the <a href="https://en.opensuse.org/openSUSE:OSC">OpenSuse command line build client</a>
    and are useful to inspect and fix hidden issues happening with remote builds like non visible compiler or linker errors.</p>

<?php include_once("footer.inc"); ?>
