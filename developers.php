<?php
    $page_title = "Umbrello Developer Resources";
    include ("lib.inc");
    include ( "header.inc" );
    function p_link($url, $text)
    {
        echo "<p><a href=\"$url\">$text</a>\n</p>";
    }

    function li_link($url, $text, $post="")
    {
        echo "<li><a href=\"$url\">$text</a>$post\n</li>";
    }

    function li_jenkins_link($text, $job)
    {
        $url = "https://build.kde.org/job/Applications/job/umbrello//job/$job";
        $icon_url ="https://build.kde.org/job/Applications/job/umbrello/job/$job/badge/icon";
        echo "<li><img style=\"margin:0px;margin-bottom:-12px\" src=\"$icon_url\">&nbsp;<a href=\"$url\">$text</a></li>";
    }
    echo getContentList(__FILE__);
?>

<h2 id="bugs">Bugs and feature requests</h2>
<p>You may take a look at the wishlist, the junior jobs and/or the open bugs:</p> 
<ul>
<?php
li_link(buglist('juniorjobs'), "Umbrello junior jobs");
li_link(buglist('wishlist'), "Wishlist");
li_link(buglist('crash'), "Crash bugs");
li_link(buglist('major'), "Grave and major bugs");
li_link(buglist('normal'), "Normal bugs");
li_link(buglist('minor'), "Minor bugs");
?>
</ul>
<h2 id="changelog">Changelog</h2>
<p>When bugs get fixed or features are implemented, the related release version is assigned to it<sup><a href="#fn1" id="ref1">1</a></sup>.
In case you want to see, which bugs has been fixed in which version you may take a look at
<ul>
<?php
$suffix = "(not released yet)";
foreach(array_keys($versions) as $version) {
    if ($version == "")
        continue;
    $release_string = $version > 19.08 ? "releases" : "Applications";
    li_link("changelog.php?$version", "Umbrello ".$versions[$version]["mapped"]." (KDE $release_string $version) Changelog $suffix");
    $suffix = "";
}
li_link("changelog.php", "Complete Changelog");
?>
</ul>
</p>
<h2 id="contribute">How to contribute</h2>
<p>
If you want to implement a feature already listed in the <a href="<?php echo buglist('wishlist') ?>">wish list</a> please:
<ol>
<li>
Create a new <a href="https://bugs.kde.org/createaccount.cgi">bug.kde.org account</a> if not available
</li>
<li>
Implement the feature and create a merge request for <a href="https://invent.kde.org/sdk/umbrello">https://invent.kde.org/sdk/umbrello</a>.
See <a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Gitlab project forking workflow</a> for hints how to do.
</li>
<li>
The related commit message should have the 'FIXED-IN:' and 'BUG:' keyword <sup><a href="#fn1" id="ref1">1</a></sup> e.g.
<pre>FIXED-IN: 2.29.3 (KDE Release 19.08.3)
BUG:12345</pre>
</li>
<li>
After review has been approved, the reviewer will merge in the patch(es) to the related branch.
</li>
</ol>
</p>
<p>
In case you are going to fix an annoying bug in umbrello listed in the above mentioned bug list please:
<ol>
<li>
Create a new <a href="https://bugs.kde.org/createaccount.cgi">bug.kde.org account</a> if not available
</li>
<li>
Assign the related bug to yourself to indicate that you are working on this bug.
</li>
<li>
Fix the bug and create a merge request for <a href="https://invent.kde.org/sdk/umbrello">https://invent.kde.org/sdk/umbrello</a>.
See <a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Gitlab project forking workflow</a> for hints how to do.
</li>
<li>
The related commit message should have the 'FIXED-IN:' and 'BUG:' keyword<sup><a href="#fn1" id="ref1">1</a></sup> e.g.
<pre>FIXED-IN: 2.29.3 (KDE Release 19.08.3)
BUG:12345</pre>
</li>
<li>
After review has been approved, the reviewer will merge in the patch(es) to the related branch.
</li>
<li>
The normal procedure is to merge stable branches into the master branch, i.e. bugs should be fixed in the stable branch.
</li>
</ol>
</p>

<h2 id="binary-factory">Binary Factory</h2>
<ul>
<li><a href="https://binary-factory.kde.org/job/Umbrello_Nightly_win64/">Win64 nightly build</a> (git master, approx 1 day delay)</li>
</ul>
<h2 id="cross-build">Windows (cross compiled)</h2>
<ul>
<li><a href="https://sourceforge.net/projects/kde-windows/files/umbrello/snapshots">Umbrello snapshot packages</a> from Open Build Service.</li>
</ul>
<h2 id="links">Links</h2>
<ul>
<?php 
li_link("https://invent.kde.org/sdk/umbrello",           "Umbrello on invent.kde.org");
li_link("https://cgit.kde.org/umbrello.git",             "Umbrello on cgit.kde.org (readonly)");
li_link("http://okellogg.de/umbrello-apidoc/index.html", "Umbrello source code documentation");
li_link("https://scan.coverity.com/projects/3327",       "Umbrello source code checking results");
li_link("https://www.openhub.net/p/umbrello/",           "Umbrello project statistics");
?>
</ul>

<h1>References</h1>
<sup id="fn1">1.</sup>This is technically achieved by the <a href="https://community.kde.org/Policies/Commit_Policy#Special_keywords_in_GIT_and_SVN_log_messages">FIXED-IN</a> keyword in the commit message.

<?php
    include ( "footer.inc" );
?>
